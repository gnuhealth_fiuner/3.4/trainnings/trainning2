# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2018 Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2018 GNU Solidario <health@gnusolidario.org>
#    Copyright (C) 2015 Cédric Krier
#    Copyright (C) 2014-2015 Chris Zimmerman <siv@riseup.net>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta

__all__ = ['Chance', 'PatientData']

##### With the metaclass=PoolMeta, we can extend a pre-existent class, as much as
##### we want
class Chance(metaclass=PoolMeta):
    "Chance"
    __name__ = 'trainning.chance'
    #######Many2One is a relation to a row on another table. Is a an external key
    owner = fields.Many2One('gnuhealth.patient','owner')
    
    end_date = fields.DateTime('End date', readonly=True)


class PatientData(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient'

    chances = fields.One2Many('trainning.chance','owner','Chances')
   
